<?php

use App\Http\HomeController;

return [
    '/' => fn() => 'routes work',
    '/controller' => [new HomeController(), 'index']
];
