<?php

return [
    'app' => [
        'session_save_path' => '/cache/session',
        'view_path' => '/views'
    ]
];