<?php

namespace Core\Http;

use Exception;

class Router
{
    protected static $routes = [];

    public static function setRoutes(string $routesPath)
    {
        self::$routes = include $routesPath;

        // вызов экземпляра из контекста стат методов
//        return new static();
    }

    /**
     * @param Request|null $request
     * @throws Exception
     */
    public static function dispatch(?Request $request)
    {
        $handler = self::$routes[$request->path()] ?? null;

        if (is_null($handler)) {
            throw new Exception('404 not found');
        }

        //можно дополнить проверку метода запроса

        return call_user_func_array($handler, []);
    }
}