<?php

namespace Core\Session;

use SessionHandlerInterface;

class SessionHandler implements SessionHandlerInterface
{
    protected string $savePath = '';
    /**
     * @inheritDoc
     */
    public function close()
    {
        // TODO: Implement close() method.
        return true;
    }

    /**
     * @inheritDoc
     */
    public function destroy($session_id)
    {
        // TODO: Implement destroy() method.
    }

    /**
     * @inheritDoc
     */
    public function gc($maxlifetime) // gurbash collector - сборка мусора
    {
        // TODO: Implement gc() method.
    }

    /**
     * @inheritDoc
     */
    public function open($save_path, $name)
    {
        // TODO: Implement open() method.
        $this->savePath = $save_path;
        if (!is_dir($this->savePath)) {
            mkdir($this->savePath, 0777, true);
        }

        return true;
    }

    /**
     * @inheritDoc
     */
    public function read($session_id)
    {
        // TODO: Implement read() method.
        $file = $this->savePath . DIRECTORY_SEPARATOR . $session_id;
        if (!file_exists($file)) {
            touch($file);
        }

        return file_get_contents($file);
    }

    /**
     * @inheritDoc
     */
    public function write($session_id, $session_data)
    {
        // TODO: Implement write() method.
        return file_put_contents($this->savePath . DIRECTORY_SEPARATOR . $session_id, $session_data) !== false;
    }
}