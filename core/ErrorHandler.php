<?php

namespace Core;

use ErrorException;
use Throwable;

class ErrorHandler
{
    public static function error($code, $message, $filename, $linenno)
    {
        throw new ErrorException($message, $code, 1, $filename, $linenno);
    }
    // Throwable покрывают все ошибки php
    public static function exception(Throwable $exception)
    {
        exit("<pre>$exception</pre>");
    }
}