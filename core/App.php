<?php

namespace Core;

use Core\Http\Router;
use Core\Session\SessionHandler;
use Core\Http\Request;

class App
{
    protected static string $root;

    protected string $configPath; //unitialized(string)
    protected string $routesPath;
    protected array $config = [];
    protected ?Request $request = null;
    /**
     * App constructor.
     */
    public function __construct()
    {
        self::$root = getcwd(); // абсолютный путь к index.php
        $this->configPath = self::$root . "config/config.php";
        $this->configPath = self::$root . "config/routes.php";
    }

    /**
     * @param string $configPath
     * @return App
     */
    public function setConfigPath(string $configPath): App
    {
        $this->configPath = self::$root . $configPath;
        return $this;
    }

    /**
     * @param string $routesPath
     * @return App
     */
    public function setRoutesPath(string $routesPath): App
    {
        $this->routesPath = self::$root . $routesPath;
        return $this;
    }

    public function run()
    {
        $response = $this->init()
            ->startSession()
            ->dispatch();
        $this->terminate($response);
    }

    // инициализация приложения
    protected function init()
    {
        $this->config = include $this->configPath;

        $this->request = Request::init();
        Router::setRoutes($this->routesPath);
        View::init(self::$root . $this->config['app']['view_path']);

        set_error_handler([ErrorHandler::class, 'error']); //при ошибке в классе ErrorHandler вызовится статичиский метод error
        set_exception_handler([ErrorHandler::class, 'exception']);


        return $this;
    }

    // работа с сессиями
    protected function startSession()
    {
        //C:\Projects\myframework/cache/session
        session_save_path(self::$root . $this->config['app']['session_save_path']);
        session_set_save_handler(new SessionHandler());
        session_start();

        $_SESSION['authorized'] ??= false;

        return $this;
    }

    // передача запроса в роутер, для его обработки
    protected function dispatch()
    {
        return Router::dispatch($this->request);
    }

    protected function terminate($response)
    {
        exit($response);
    }
}