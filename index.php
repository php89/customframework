<?php

use Core\App;

require_once __DIR__ . '/vendor/autoload.php';

$app = new App();

$app->setConfigPath('/config/config.php')
    ->setRoutesPath('/config/routes.php');

$app->run();
